# issues
“Displays the list of open `issues` on github

    This code is based on the similarly named programs from the book
    "The Go Programming Language" by Alan Donovan, and Brian Kernighan.

## Sample Programs

**issues usage**

``` bash
# Displays the github issues
$ ./issues repo:golang/go is:open json decoder
issues application
35 issues:
#31701    lr1980 encoding/json: second decode after error impossible
#29688   sheerun proposal: encoding/json: add InputOffset to json decode
#31789  mgritter encoding/json: provide a way to limit recursion depth
#29686   sheerun json: Add InputOffset for stream byte offset access
#28923     mvdan encoding/json: speed up the decoding scanner
#11046     kurin encoding/json: Decoder internally buffers full input
$
```
